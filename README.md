myCRUD Template
==============

Ce tp consiste à réaliser une simple application web de gestion de livres.

Pré-requis
============

    $ sudo apt-get install git virtualenv python3-virtualenv python3-pip sqlitebrowser
    $ git clone https://gitlab.com/blewandoski/tp-flask-skeleton.git
    $ cd tp-flask-skeleton
    $ virtualenv --python=/usr/bin/python3 venv
    $ source venv/bin/activate
    $ pip3 install -r requirements.txt

Déroulement du TP
===========

    Vous pouvez suivre les instructions contenues dans le PDF

Exécution
===========

    $ python3 myApp.py

Liens Utiles
===========

    https://docs.python.org/fr/3

    https://flask.palletsprojects.com/en/1.1.x/

    https://flask.palletsprojects.com/en/1.1.x/quickstart/
