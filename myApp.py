import os

from flask import Flask, render_template, request, redirect, session, flash

from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

# Config BDD

project_dir = os.path.dirname(os.path.abspath(__file__))
uri_database = "sqlite:///{}".format(os.path.join(project_dir, "myApp.db"))
app.config["SQLALCHEMY_DATABASE_URI"] = uri_database
app.config["SQLALCHEMY_BINDS"] = {
    'users': "sqlite:///users.db",
    # TO-DO : 1.2
}
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

# Modèles pour nos deux bases de données

class User(db.Model):
    __bind_key__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    password = db.Column(db.String(80))

    def __init__(self, username, password):
        self.username = username
        self.password = password

# TO-DO : 1.1
# TO-DO : 1.2

# Route de base

@app.route("/", methods=["GET", "POST"])
def index():
    return render_template("index.html")

# Toutes les routes concernant l'authentification

@app.route("/login", methods=["GET", "POST"])
def login():
    try:
        name = request.form['username']
        passw = request.form['password']
        data = User.query.filter_by(username=name, password=passw).first()
        if data is not None:
            session['logged_in'] = True
            books = Book.query.all()
            return render_template("home.html", books=books)
        else:
            return redirect("/")
    except:
        return redirect("/")

# TO-DO 3.1
# TO-DO 3.2

@app.route("/logout", methods=["GET", "POST"])
def logout():
    session['logged_in'] = False
    return redirect("/")

# Toutes les routes pour le CRUD des livres

# TO-DO 4.

@app.route("/home", methods=["GET", "POST"])
def home():
    books = None
    books = Book.query.all()
    return render_template("home.html", books=books)

# Démarrage de l'app

if __name__ == "__main__":
    #Création des dbs
    db.drop_all()
    db.create_all()

    #Insertion de quelques dummies values
    toto = User("toto", "toto")
    tata = User("tata", "tata")
    # lotr = Book("Le seigneur des anneaux")
    db.session.add(toto)
    db.session.add(tata)
    # db.session.add(lotr)
    db.session.commit()

    #Clef secrète pour le login
    app.secret_key = "123"

    app.run(host='127.0.0.1', port=8080, debug=True)
